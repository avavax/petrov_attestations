package com.innopolis;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UsersRepositoryFileImpl implements UsersRepository {

    @FunctionalInterface
    private interface ByCondition {
        boolean isOk(String name, int age, boolean isWorker);
    }

    private final String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void save(User user) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, true))) {
            bufferedWriter.write(user.getId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User user) {
        Map<Integer, User> users = findByCondition((name, age, isWorker) -> true);
        users.put(user.getId(), user);
        this.saveAll(users);
    }

    @Override
    public User findById(int id) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {

            String line = bufferedReader.readLine();

            while (line != null) {
                String[] parts = line.split("\\|");
                int currentId = Integer.parseInt(parts[0]);

                if (currentId == id) {
                    String name = parts[1];
                    int age = Integer.parseInt(parts[2]);
                    boolean isWorker = Boolean.parseBoolean(parts[3]);
                    return new User(id, name, age, isWorker);
                }

                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }

    @Override
    public List<User> findAll() {
        return mapToList(findByCondition((name, age, isWorker) -> true));
    }

    @Override
    public List<User> findByAge(int age) {
        return mapToList(findByCondition((name, itemAge, isWorker) -> itemAge == age));
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        return mapToList(findByCondition((name, age, isWorker) -> isWorker));
    }

    private Map<Integer, User> findByCondition(ByCondition condition) {
        Map<Integer, User> users = new HashMap<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {

            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);

                if (condition.isOk(name, age, isWorker)) {
                    users.put(id, new User(id, name, age, isWorker));
                }
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    private void saveAll(Map<Integer, User> users) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false))) {
            for (User user : users.values()) {
                bufferedWriter.write(user.getId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.isWorker());
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private List<User> mapToList(Map<Integer, User> users) {
        return new ArrayList<> (users.values());
    }
}
