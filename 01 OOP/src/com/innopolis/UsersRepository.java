package com.innopolis;

import java.util.List;

public interface UsersRepository {
    void save(User user);
    void update (User user);
    User findById(int id);
    List<User> findAll();
    List<User> findByAge(int age);
    List<User> findByIsWorkerIsTrue();
}
