package com.innopolis;

public class MainApp {
    public static void main(String[] args) {
        UsersRepository userRepository = new UsersRepositoryFileImpl("users.txt");

        // Получить всех пользователей
        userRepository.findAll().forEach(System.out::println);
        System.out.println();

        // Получить список по возрасту
        userRepository.findByAge(38).forEach(System.out::println);
        System.out.println();

        // Получить список работающих
        userRepository.findByIsWorkerIsTrue().forEach(System.out::println);
        System.out.println();

        // Получить пользователя по id
        User testUser = userRepository.findById(5);
        System.out.println(testUser);
        System.out.println();

        // Изменить поля пользователя
        testUser.setName("Никанор");
        testUser.setAge(26);
        userRepository.update(testUser);
        System.out.println(userRepository.findById(5));
    }
}
